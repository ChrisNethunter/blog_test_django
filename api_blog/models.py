from django.db import models
from django.contrib.auth.models import User

class Blogs(models.Model):
    STATES = [
        (True, 'active'),
        (False, 'inactive'),
    ]
    title = models.CharField(max_length=30)
    description = models.CharField(max_length=30)
    createdAt = models.DateField()
    status = models.BooleanField(
        choices=STATES,
        default='active',
    )


class Votes(models.Model):
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
    )
    blog = models.ForeignKey(
        'api_blog.Blogs',
        on_delete=models.CASCADE,
    )
    createdAt = models.DateField()
